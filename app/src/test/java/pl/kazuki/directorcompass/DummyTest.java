package pl.kazuki.directorcompass;

import static junit.framework.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import pl.kazuki.directorcompass.BuildConfig;

/**
 * Created by konczynskip on 6/7/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 18, manifest = "src/main/AndroidManifest.xml")
public class DummyTest {

    @Before
    public void setUp() {
        String test = "test";
    }

    @Test
    public void testDummy() {
        assertTrue(true);
    }

    @Test
    public void testDummy1() {
        assertFalse(false);
    }
}

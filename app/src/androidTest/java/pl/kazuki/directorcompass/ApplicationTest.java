package pl.kazuki.directorcompass;

import android.app.Instrumentation;
import android.hardware.GeomagneticField;
import android.location.Location;
import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.widget.Button;

import pl.kazuki.directorcompass.customViews.CompassView;
import pl.kazuki.directorcompass.customViews.DirectionArrowView;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ActivityInstrumentationTestCase2<MainActivity>{

    //0-90
    private double CUSTOM_LATITUDE = 1d;
    //0-180
    private double CUSTOM_LONGITUDE = 0d;

    private MainActivity mMainActivity;
    private CompassView mCompassView;
    private DirectionArrowView mDirArrow;
    private Button mLatitudeButton;
    private Button mLongitudeButton;

    public ApplicationTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        setActivityInitialTouchMode(true);
        mMainActivity = getActivity();
        mCompassView = (CompassView) mMainActivity.findViewById(R.id.compassView);
        mDirArrow = (DirectionArrowView) mMainActivity.findViewById(R.id.directionArrow);
        mLatitudeButton = (Button) mMainActivity.findViewById(R.id.latitudeButton);
        mLongitudeButton = (Button) mMainActivity.findViewById(R.id.longitudeButton);
    }

    @UiThreadTest
    public void testStateAfterPause() {
        Instrumentation mInstrument = getInstrumentation();
        mMainActivity.OnLongitudeChange(CUSTOM_LONGITUDE, true);
        mMainActivity.OnLatitudeChange(CUSTOM_LATITUDE, true);

        mInstrument.callActivityOnPause(mMainActivity);
        mInstrument.callActivityOnResume(mMainActivity);

        Location customDestination = mMainActivity.getmDestination();
        assertEquals(CUSTOM_LATITUDE, customDestination.getLatitude());
        assertEquals(CUSTOM_LONGITUDE, customDestination.getLongitude());
    }

    @UiThreadTest
    public void testLocationUpdateForEastDirection() {
        mMainActivity.OnLongitudeChange(1.0d, true);
        mMainActivity.OnLatitudeChange(0.0d, true);

        Location fakeLocation = new Location("");
        mMainActivity.onLocationChanged(fakeLocation);

        GeomagneticField mGeomagneticField = new GeomagneticField((float) fakeLocation.getLatitude(),
                (float) fakeLocation.getLongitude(),
                (float) fakeLocation.getAltitude(),
                System.currentTimeMillis());

        float dirAzimut = mDirArrow.getDirAzimuthInDegrees();
        assertEquals(90f,dirAzimut -= mGeomagneticField.getDeclination());
    }

    @UiThreadTest
    public void testLocationUpdateForNorthDirection() {
        mMainActivity.OnLongitudeChange(0.d, true);
        mMainActivity.OnLatitudeChange(10d, true);

        Location fakeLocation = new Location("");
        mMainActivity.onLocationChanged(fakeLocation);

        GeomagneticField mGeomagneticField = new GeomagneticField((float) fakeLocation.getLatitude(),
                (float) fakeLocation.getLongitude(),
                (float) fakeLocation.getAltitude(),
                System.currentTimeMillis());

        float dirAzimut = mDirArrow.getDirAzimuthInDegrees();
        assertEquals(0f,dirAzimut -= mGeomagneticField.getDeclination());
    }

    @UiThreadTest
    public void testLocationUpdateForWest() {
        mMainActivity.OnLongitudeChange(1f, false);
        mMainActivity.OnLatitudeChange(0.0d, false);

        Location fakeLocation = new Location("");
        mMainActivity.onLocationChanged(fakeLocation);

        GeomagneticField mGeomagneticField = new GeomagneticField((float) fakeLocation.getLatitude(),
                (float) fakeLocation.getLongitude(),
                (float) fakeLocation.getAltitude(),
                System.currentTimeMillis());

        float dirAzimut = mDirArrow.getDirAzimuthInDegrees();
        assertEquals(-90f,dirAzimut -= mGeomagneticField.getDeclination());
    }

    @UiThreadTest
    public void testLocationUpdateForSouth() {
        mMainActivity.OnLongitudeChange(0d, true);
        mMainActivity.OnLatitudeChange(10d, false);

        Location fakeLocation = new Location("");
        mMainActivity.onLocationChanged(fakeLocation);

        GeomagneticField mGeomagneticField = new GeomagneticField((float) fakeLocation.getLatitude(),
                (float) fakeLocation.getLongitude(),
                (float) fakeLocation.getAltitude(),
                System.currentTimeMillis());

        float dirAzimut = mDirArrow.getDirAzimuthInDegrees();
        assertEquals(180f,dirAzimut -= mGeomagneticField.getDeclination());
    }

    @UiThreadTest
    public void testStateAfterDestroy() {
        Instrumentation mInstrument = getInstrumentation();
        mMainActivity.OnLongitudeChange(CUSTOM_LONGITUDE, true);
        mMainActivity.OnLatitudeChange(CUSTOM_LATITUDE, true);

        mMainActivity.finish();

        mMainActivity = getActivity();
        mInstrument.callActivityOnResume(mMainActivity);
        Location customDestination = mMainActivity.getmDestination();
        assertEquals(CUSTOM_LATITUDE, customDestination.getLatitude());
        assertEquals(CUSTOM_LONGITUDE, customDestination.getLongitude());
    }
}
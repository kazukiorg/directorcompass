package pl.kazuki.directorcompass.services;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by Sylwia on 2015-05-20.
 */
public class LocationService implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String DIALOG_ERROR = "dialog_error";
    public static final int REQUEST_RESOLVE_ERROR = 6969;

    private boolean mResolving;
    private final LocationListener mListener;
    private final Context mContext;
    private final GoogleApiClient mGoogleApiClient;
    private final LocationRequest mLocationRequest;

    public <T extends Context & LocationListener> LocationService(T context, boolean errorResolvingFlag) {
        this(context, context, errorResolvingFlag);
    }

    public LocationService(Context context, LocationListener listener, boolean errorResolvingFlag) {
        mContext = context;
        mListener = listener;
        mResolving = errorResolvingFlag;

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5 * 1000)
                .setFastestInterval(2 * 1000);

    }

    public void start() {
        mGoogleApiClient.connect();
    }

    public void stop() {
        if (mGoogleApiClient.isConnected()){
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,mListener);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            mListener.onLocationChanged(location);
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,mListener);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (mResolving) {
            return;
        } else {
            mResolving = true;
            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult((Activity)mContext, REQUEST_RESOLVE_ERROR);
                } catch (IntentSender.SendIntentException e) {
                    mGoogleApiClient.connect();
                }
            } else {
                Toast.makeText(mContext,"Api error: " + connectionResult.getErrorCode() ,Toast.LENGTH_SHORT).show();
            }
        }
    }

    public boolean isConnecting() {
        return mGoogleApiClient.isConnecting();
    }

    public boolean isConnected() {
        return mGoogleApiClient.isConnected();
    }
}

package pl.kazuki.directorcompass.services;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by Sylwia on 2015-05-20.
 */
public class MagneticCompassService implements SensorEventListener {

    private final MagneticCompassEvent eventHandler;
    private final SensorManager mSensorManager;
    private final Sensor mAccel;
    private final Sensor mMagnet;
    private float[] rotationMatrix = new float[9];
    private float[] orientation = new float[3];
    private float[] mGravityMatrix = new float[3];
    private float[] mMagneticMatrix = new float[3];

    public interface MagneticCompassEvent {
        void onMagneticCompassUpdate(float magneticAzimuthInDegrees);
    }

    public <T extends Context & MagneticCompassEvent> MagneticCompassService(T appContext) {
        this(appContext, appContext);
    }

    public MagneticCompassService(Context appContext, MagneticCompassEvent handler) {
        eventHandler = handler;
        mSensorManager = (SensorManager) appContext.getSystemService(appContext.SENSOR_SERVICE);
        mAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnet = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    public void registerListeners() {
        mSensorManager.registerListener(this, mAccel, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mMagnet, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unregisterListeners() {
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                System.arraycopy(event.values, 0, mGravityMatrix, 0, event.values.length);
                break;

            case Sensor.TYPE_MAGNETIC_FIELD:
                System.arraycopy(event.values, 0, mMagneticMatrix, 0, event.values.length);
                break;
        }

        float compassDirection = this.getCompassDirection();
        eventHandler.onMagneticCompassUpdate(compassDirection);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    private float getCompassDirection() {
        if (mGravityMatrix != null && mMagneticMatrix != null) {
            if (SensorManager.getRotationMatrix(rotationMatrix, null, mGravityMatrix, mMagneticMatrix)) {
                SensorManager.getOrientation(rotationMatrix, orientation);
                double orientationInDegrees = Math.toDegrees(orientation[0]);
                return -(float) (orientationInDegrees+360)%360;
            }
        }
        return 0;
    }
}

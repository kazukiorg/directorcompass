package pl.kazuki.directorcompass;

import android.content.Intent;
import android.hardware.GeomagneticField;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pl.kazuki.directorcompass.customViews.CompassArrowView;
import pl.kazuki.directorcompass.customViews.CompassView;
import pl.kazuki.directorcompass.customViews.DirectionArrowView;
import pl.kazuki.directorcompass.helpers.BundlesKeys;
import pl.kazuki.directorcompass.listners.OnLatitudeClickListener;
import pl.kazuki.directorcompass.listners.OnLongitudeClickListener;
import pl.kazuki.directorcompass.services.LocationService;
import pl.kazuki.directorcompass.services.MagneticCompassService;

public class MainActivity extends FragmentActivity implements MagneticCompassService.MagneticCompassEvent,
        OnLatitudeClickListener.OnLatitudeChangeListener,
        OnLongitudeClickListener.OnLongitudeChangeListner,
        LocationListener{

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final boolean DEBUG = true;

    @InjectView(R.id.longitudeButton)
    Button mLongitudeBtn;
    @InjectView(R.id.latitudeButton)
    Button mLatitudeBtn;

    private LocationService mGPS;
    private Location mDestination;
    public Location getmDestination() {
        return mDestination;
    }

    private Location mLastLocation;
    private GeomagneticField mGeomagneticField;

    private MagneticCompassService mCompassHelpers;
    @InjectView(R.id.compassView)
    CompassView mCompassView;

    @InjectView(R.id.compassArrow)
    CompassArrowView mCompassArrowView;

    @InjectView(R.id.directionArrow)
    DirectionArrowView mDirectionArrowView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        OnLatitudeClickListener latitudeClickListener = new OnLatitudeClickListener(this);
        latitudeClickListener.setDefaultValue(0);
        mLatitudeBtn.setOnClickListener(latitudeClickListener);

        OnLongitudeClickListener longitudeClickListener = new OnLongitudeClickListener(this);
        longitudeClickListener.setDefaultValue(0);
        mLongitudeBtn.setOnClickListener(longitudeClickListener);

        mCompassHelpers = new MagneticCompassService(this);

        mResolving = savedInstanceState != null && savedInstanceState.getBoolean(BundlesKeys.RESOLVING_ERROR);
        mGPS = new LocationService(this, mResolving);
        mDestination = new Location("");
        if (savedInstanceState!= null) {
            mDestination.setLatitude(savedInstanceState.getFloat(BundlesKeys.LATITUDE_VALUE));
            mDestination.setLongitude(savedInstanceState.getFloat(BundlesKeys.LONGITUDE_VALUE));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCompassHelpers.registerListeners();
        mGPS.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCompassHelpers.unregisterListeners();
        mGPS.stop();
    }

    Float lastMagneticAzimut = 0f;
    @Override
    public void onMagneticCompassUpdate(float magneticAzimuthInDegrees) {
        if (Math.abs(lastMagneticAzimut-magneticAzimuthInDegrees) > 1f) {
            lastMagneticAzimut = magneticAzimuthInDegrees;
            if (mGeomagneticField != null) {
                magneticAzimuthInDegrees += mGeomagneticField.getDeclination();
            }

            mCompassArrowView.setNorthAzimutInDegrees(magneticAzimuthInDegrees);
            mDirectionArrowView.setNorthAzimutInDegrees(magneticAzimuthInDegrees);
        }
    }

    private boolean mResolving = false;
    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putBoolean(BundlesKeys.RESOLVING_ERROR, mResolving);
        outState.putFloat(BundlesKeys.LATITUDE_VALUE, (float) mDestination.getLatitude());
        outState.putFloat(BundlesKeys.LONGITUDE_VALUE, (float) mDestination.getLongitude());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == mGPS.REQUEST_RESOLVE_ERROR) {
            mResolving = false;
            if (resultCode == RESULT_OK) {
                if (!mGPS.isConnecting() && !mGPS.isConnected()) {
                    mGPS.start();
                }
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    private void handleNewLocation(Location location) {

        mGeomagneticField = new GeomagneticField(
                (float)location.getLatitude(),
                (float)location.getLongitude(),
                (float)location.getAltitude(),
                    System.currentTimeMillis());

        float destinationAzimut = location.bearingTo(mDestination);
        destinationAzimut += mGeomagneticField.getDeclination();
        mDirectionArrowView.setDirAzimut(destinationAzimut);
        mLastLocation = location;
    }

    @Override
    public void OnLatitudeChange(double latitude, boolean isOnEastHemiSphere) {
        if (!isOnEastHemiSphere)
            latitude = -latitude;
        mDestination.setLatitude(latitude);
        Toast.makeText(this, "Lat: " + mDestination.getLatitude() + ", Long: " + mDestination.getLongitude(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void OnLongitudeChange(double longitude, boolean isOnNorhtHemiSphere) {
        if (!isOnNorhtHemiSphere)
            longitude = -longitude;
        mDestination.setLongitude(longitude);
        Toast.makeText(this, "Lat: " + mDestination.getLatitude() + ", Long: " + mDestination.getLongitude(), Toast.LENGTH_SHORT).show();
    }
}

package pl.kazuki.directorcompass.listners;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.common.base.Strings;

import pl.kazuki.directorcompass.R;

/**
 * Created by Sylwia on 2015-05-21.
 */
public class OnLatitudeClickListener implements View.OnClickListener {

    private Context mContext;
    private OnLatitudeChangeListener mListener;
    private String lastEntry;
    private boolean isLastEntryOnEastHemiSphere;

    public interface OnLatitudeChangeListener {
        void OnLatitudeChange(double latitude, boolean isOnEastHemiSphere);
    }

    public <T extends Context & OnLatitudeChangeListener> OnLatitudeClickListener(T context) {
        this(context, context);
    }

    public OnLatitudeClickListener(Context context, OnLatitudeChangeListener listener) {
        mContext = context;
        mListener = listener;
    }

    public void setDefaultValue(float value) {
        lastEntry = Float.toString(value);
        isLastEntryOnEastHemiSphere = value >= 0f;
    }

    @Override
    public void onClick(View v) {
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View view = inflater.inflate(R.layout.latitude_dialog, null);

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(mContext)
                .setTitle(R.string.latitude)
                .setMessage(R.string.change_latitude)
                .setView(view);


        final AlertDialog alertDialog = alertBuilder.setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, null).create();

        final EditText mEdit = (EditText) view.findViewById(R.id.latitude_dialog_edittest);
        final ToggleButton mToggle = (ToggleButton) view.findViewById(R.id.latitude_hemisphere);


        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                mEdit.setText(lastEntry);
                mToggle.setChecked(isLastEntryOnEastHemiSphere);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String latitude = mEdit.getText().toString();
                        if (!Strings.isNullOrEmpty(latitude)) {
                            double latitudeValue = Double.valueOf(latitude);
                            if (latitudeValue >= 0.0 && latitudeValue <= 90) {
                                alertDialog.dismiss();
                                lastEntry = latitude;
                                isLastEntryOnEastHemiSphere = mToggle.isChecked();
                                mListener.OnLatitudeChange(latitudeValue, mToggle.isChecked());
                                return;
                            }
                        }

                        Toast.makeText(mContext, R.string.error_latitude_invalid_value, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        alertDialog.show();

    }
}

package pl.kazuki.directorcompass.listners;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.common.base.Strings;

import pl.kazuki.directorcompass.R;

/**
 * Created by Sylwia on 2015-05-21.
 */
public class OnLongitudeClickListener implements View.OnClickListener {

    private Context mContext;
    private OnLongitudeChangeListner mListener;
    private String lastEntry;
    private boolean isLastEntryOnSouthHemiSphere;

    public interface OnLongitudeChangeListner {
        void OnLongitudeChange(double longitude, boolean isOnNorthHemiSphere);
    }

    public <T extends Context & OnLongitudeChangeListner> OnLongitudeClickListener(T context) {
        this(context, context);
    }

    public OnLongitudeClickListener(Context context, OnLongitudeChangeListner listner) {
        this.mContext = context;
        this.mListener = listner;
    }

    public void setDefaultValue(float value) {
        lastEntry = Float.toString(value);
        isLastEntryOnSouthHemiSphere = value >= 0f;
    }

    @Override
    public void onClick(View v) {
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View view = inflater.inflate(R.layout.longitude_dialog, null);

        AlertDialog.Builder mAlertBuilder = new AlertDialog.Builder(mContext)
        .setTitle(R.string.latitude)
        .setMessage(R.string.change_latitude)
        .setView(view);

        final EditText mEdit = (EditText) view.findViewById(R.id.longitude_dialog_edittest);
        final ToggleButton mToggle = (ToggleButton) view.findViewById(R.id.longitude_hemisphere);

        final AlertDialog alarmDialog = mAlertBuilder.setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, null).create();

        alarmDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button positiveButton = alarmDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                mEdit.setText(lastEntry);
                mToggle.setChecked(isLastEntryOnSouthHemiSphere);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String longitude = mEdit.getText().toString();
                        if (!Strings.isNullOrEmpty(longitude)) {
                            double longitudeValue = Double.valueOf(longitude);
                            if (longitudeValue >= 0 && longitudeValue <= 180) {
                                alarmDialog.dismiss();
                                lastEntry = longitude;
                                isLastEntryOnSouthHemiSphere = mToggle.isChecked();
                                mListener.OnLongitudeChange(longitudeValue, mToggle.isChecked());
                                return;
                            }
                        }

                        Toast.makeText(mContext, R.string.error_longitude_invalid_value, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        alarmDialog.show();
    }
}

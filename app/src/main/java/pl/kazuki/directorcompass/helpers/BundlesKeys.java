package pl.kazuki.directorcompass.helpers;

/**
 * Created by Sylwia on 2015-05-26.
 */
public class BundlesKeys {
    public static String LATITUDE_VALUE = "destination_latitude_value";
    public static String LONGITUDE_VALUE = "destination_longitude_value";
    public static String RESOLVING_ERROR = "resolving_api_error";
}

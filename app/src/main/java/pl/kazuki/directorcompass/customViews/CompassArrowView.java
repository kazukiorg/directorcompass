package pl.kazuki.directorcompass.customViews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Sylwia on 2015-05-27.
 */
public class CompassArrowView extends View {
    private Paint mSouthDirectionPaint;
    private Paint mSouthDarkDirectionPaint;
    private Paint mNorthDirectionPaint;
    private Paint mNorthDarkDirectionPaint;
    private Path northDarkPath;
    private Path northLightPath;
    private Path southDarkPath;
    private Path southLightPath;
    private int centerX;
    private int centerY;

    public CompassArrowView(Context context) {
        super(context);
        init();
    }

    public CompassArrowView(Context context, AttributeSet attr) {
        super(context, attr);
        init();
    }

    private Float northAzimut = 0f;
    public Float getNorthAzimutInDegrees() {
        return northAzimut;
    }

    public void setNorthAzimutInDegrees(Float azimut) {
        this.northAzimut = azimut;
        invalidate();
    }

    private void init() {
        mSouthDirectionPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mSouthDirectionPaint.setColor(Color.RED);

        mSouthDarkDirectionPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mSouthDarkDirectionPaint.setColor(Color.rgb(220,0,0));

        mNorthDirectionPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mNorthDirectionPaint.setColor(Color.BLUE);

        mNorthDarkDirectionPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mNorthDarkDirectionPaint.setColor(Color.rgb(0,0,225));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(
                MeasureSpec.getSize(widthMeasureSpec),
                MeasureSpec.getSize(heightMeasureSpec));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w,h,oldw,oldh);

        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        int radius = Math.min(width,height)/2;
        centerX = width/2;
        centerY = height/2;
        float southNorthRadiusScaled = radius * 0.7f;
        float eastWestRadiusScaled = radius * 0.1f;
        int northPeek = (int) (centerY-southNorthRadiusScaled);
        int southPeek = (int) (centerY+southNorthRadiusScaled);
        int westPeek = (int) (centerX-eastWestRadiusScaled);
        int eastPeek = (int) (centerX+eastWestRadiusScaled);

        northDarkPath = new Path();
        northDarkPath.setFillType(Path.FillType.EVEN_ODD);
        northDarkPath.moveTo(centerX, centerY);
        northDarkPath.lineTo(westPeek, centerY);
        northDarkPath.lineTo(centerX, northPeek);
        northDarkPath.close();

        northLightPath = new Path();
        northLightPath.setFillType(Path.FillType.EVEN_ODD);
        northLightPath.moveTo(centerX, centerY);
        northLightPath.lineTo(eastPeek, centerY);
        northLightPath.lineTo(centerX, northPeek);
        northLightPath.close();

        southDarkPath = new Path();
        southDarkPath.setFillType(Path.FillType.EVEN_ODD);
        southDarkPath.moveTo(centerX, centerY);
        southDarkPath.lineTo(eastPeek, centerY);
        southDarkPath.lineTo(centerX, southPeek);
        southDarkPath.close();

        southLightPath = new Path();
        southLightPath.setFillType(Path.FillType.EVEN_ODD);
        southLightPath.moveTo(centerX, centerY);
        southLightPath.lineTo(westPeek, centerY);
        southLightPath.lineTo(centerX, southPeek);
        southLightPath.close();
    }

    protected void onDraw(Canvas canvas) {
        canvas.rotate(getNorthAzimutInDegrees(), centerX, centerY);

        canvas.drawPath(northLightPath, mNorthDirectionPaint);
        canvas.drawPath(northDarkPath, mNorthDarkDirectionPaint);
        canvas.drawPath(southDarkPath, mSouthDarkDirectionPaint);
        canvas.drawPath(southLightPath, mSouthDirectionPaint);
    }
}

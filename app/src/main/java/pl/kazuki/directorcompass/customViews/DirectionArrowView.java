package pl.kazuki.directorcompass.customViews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Sylwia on 2015-05-26.
 */
public class DirectionArrowView extends View {

    private Paint mArrowPlatePaint;
    private Paint mArrowPointerPaint;
    private int mCenterX;
    private int mCenterY;
    private float mCircleCenterX;
    private float mCircleCenterY;
    private float mMainPlateRadius;
    private float mPointerRadius;
    private float mPointerYPeek;

    public DirectionArrowView(Context context) {
        super(context);
        init();
    }

    public DirectionArrowView(Context context, AttributeSet attr) {
        super(context, attr);
        init();
    }

    private Float dirAzimuth = 45f;
    public Float getDirAzimuthInDegrees() {
        return dirAzimuth;
    }

    public void setDirAzimut(Float azimut) {
        this.dirAzimuth = azimut;
        invalidate();
    }

    private Float northAzimut = 0f;
    public Float getNorthAzimutInDegrees() {
        return northAzimut;
    }

    public void setNorthAzimutInDegrees(Float azimut) {
        this.northAzimut = azimut;
        invalidate();
    }

    private void init() {
        mArrowPlatePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mArrowPlatePaint.setAlpha(30);
        mArrowPlatePaint.setColor(Color.argb(100, 255, 255, 255));

        mArrowPointerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mArrowPointerPaint.setColor(Color.RED);
        mArrowPointerPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(
                MeasureSpec.getSize(widthMeasureSpec),
                MeasureSpec.getSize(heightMeasureSpec));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w,h,oldw,oldh);

        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        int radius = Math.min(width, height) / 2;
        mCenterX = width / 2;
        mCenterY = height / 2;

        mCircleCenterX = (float) (mCenterX + (Math.sin(0) * radius * 0.85));
        mCircleCenterY = (float) (mCenterY - (Math.cos(0) * radius * 0.85));
        mMainPlateRadius = radius * 0.1f;
        mPointerRadius = mMainPlateRadius * 0.2f;
        mPointerYPeek = mCircleCenterY - mMainPlateRadius / 2;
    }

    protected void onDraw(Canvas canvas) {
        canvas.rotate(getDirAzimuthInDegrees() + getNorthAzimutInDegrees(), mCenterX, mCenterY);

        canvas.drawCircle(mCircleCenterX, mCircleCenterY, mMainPlateRadius, mArrowPlatePaint);
        canvas.drawCircle(mCircleCenterX, mPointerYPeek, mPointerRadius, mArrowPointerPaint);
    }
}

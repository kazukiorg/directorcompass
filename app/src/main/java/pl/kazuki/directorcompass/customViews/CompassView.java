package pl.kazuki.directorcompass.customViews;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import pl.kazuki.directorcompass.R;

/**
 * Created by Sylwia on 2015-05-22.
 */
public class CompassView extends View {

    private Paint mMayorDirectionPaint;
    private Paint mMinorDirectionPaint;
    private Paint mSouthDirectionPaint;
    private Paint mNorthDirectionPaint;
    private Paint mCompassPaint;
    private Paint mCompassBorderPaint;
    private RadialGradient mCompassGradient;

    private String north;
    private String northWest;
    private String northEast;
    private String south;
    private String southWest;
    private String southEast;
    private String east;
    private String west;
    private int centerX;
    private int centerY;
    private int radius;
    private float top;
    private float borderRadius;

    public CompassView(Context context) {
        super(context);
        init(context);
    }

    public CompassView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        Resources res = context.getResources();
        north = res.getString(R.string.north);
        south = res.getString(R.string.south);
        east = res.getString(R.string.east);
        west = res.getString(R.string.west);
        northEast = res.getString(R.string.north_east);
        northWest = res.getString(R.string.north_west);
        southEast = res.getString(R.string.south_east);
        southWest = res.getString(R.string.south_west);

        mMayorDirectionPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mMayorDirectionPaint.setColor(Color.WHITE);
        mMayorDirectionPaint.setTextSize(45);

        mMinorDirectionPaint = new Paint(mMayorDirectionPaint);
        mMinorDirectionPaint.setTextSize(30);

        mSouthDirectionPaint = new Paint(mMayorDirectionPaint);
        mSouthDirectionPaint.setColor(Color.RED);

        mNorthDirectionPaint = new Paint(mMayorDirectionPaint);
        mNorthDirectionPaint.setColor(Color.BLUE);

        mCompassPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCompassBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCompassBorderPaint.setColor(Color.GRAY);
        mCompassBorderPaint.setStyle(Paint.Style.STROKE);
        mCompassBorderPaint.setStrokeWidth(15);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(
                MeasureSpec.getSize(widthMeasureSpec),
                MeasureSpec.getSize(heightMeasureSpec));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        radius = Math.min(width,height)/2;
        centerX = width/2;
        centerY = height/2;
        borderRadius = radius - mCompassBorderPaint.getStrokeWidth() / 2;

        top = centerY - radius * 0.8f;

        if (mCompassGradient == null) {
            mCompassGradient = new RadialGradient(centerX, centerY, radius * 1.1f, Color.rgb(102, 255, 255), Color.rgb(0, 128, 255), Shader.TileMode.CLAMP);
            mCompassPaint.setShader(mCompassGradient);
        }
    }

    protected void onDraw(Canvas canvas) {

        canvas.drawCircle(centerX, centerY, radius, mCompassPaint);
        canvas.drawCircle(centerX, centerY, borderRadius, mCompassBorderPaint);

        canvas.save();
        canvas.drawText(north, centerX, top, mNorthDirectionPaint);
        canvas.rotate(45, centerX, centerY);
        canvas.drawText(northEast, centerX, top, mMayorDirectionPaint);
        canvas.rotate(45, centerX, centerY);
        canvas.drawText(east, centerX, top, mMayorDirectionPaint);
        canvas.rotate(45, centerX, centerY);
        canvas.drawText(southEast, centerX, top, mMayorDirectionPaint);
        canvas.rotate(45, centerX, centerY);
        canvas.drawText(south, centerX, top, mSouthDirectionPaint);
        canvas.rotate(45, centerX, centerY);
        canvas.drawText(southWest, centerX, top, mMayorDirectionPaint);
        canvas.rotate(45, centerX, centerY);
        canvas.drawText(west, centerX, top, mMayorDirectionPaint);
        canvas.rotate(45, centerX, centerY);
        canvas.drawText(northWest, centerX, top, mMayorDirectionPaint);
        canvas.restore();
    }
}
